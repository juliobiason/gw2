#!/usr/bin/python
# -*- encoding: utf-8 -*-

import yaml
import argparse
import textwrap


def load_data():
    data = yaml.load(file('collectibles.yaml'))
    return data


def load_known(filename):
    data = yaml.load(file(filename))
    return data


def args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--discipline',
                        dest='discipline',
                        metavar="DISCIPLINE",
                        type=str,
                        nargs='?')
    parser.add_argument('--list',
                        dest='list',
                        metavar='FILE',
                        type=str,
                        nargs='?')
    args = parser.parse_args()

    return args


def discipline_levels(disc, data):
    levels = {}

    for mat in data:
        disciplines = data[mat]
        for discipline in disciplines:
            mat_disc = discipline['discipline']
            mat_level = discipline['level']

            if not disc == mat_disc:
                continue

            if not mat_level in levels:
                levels[mat_level] = []
            levels[mat_level].append(mat)

    return levels


def show_levels(levels):
    for level in sorted(levels.keys()):
        print '{:-3} '.format(level) + ('-' * 70)

        for mat in levels[level]:
            print mat


def find_unused(known, data):
    needed = set()
    for disc in known:
        levels = discipline_levels(disc, data)

        level_ranges = []
        previous = None
        for level in sorted(levels.keys()):
            if len(levels[level]) < 3:
                continue

            if previous:
                level_ranges.append(range(previous, level))

            previous = level

        level_ranges.append(range(400, 401))

        # print '{:<30}'.format(disc),
        # for rang in level_ranges:
        #     print rang[0], rang[-1], ',',
        # print

        disc_level = known[disc]
        should_include = False
        disc_needed = set()
        for level in level_ranges:
            if disc_level in level:
                should_include = True

            if should_include:
                base_level = level[0]
                disc_needed.update(levels[base_level])

        # print disc, disc_needed
        needed.update(disc_needed)

    # print needed
    all_mats = set(data.keys())
    dont_need = all_mats.difference(needed)
    text = ', '.join(sorted(dont_need))
    for line in textwrap.wrap(text, 75):
        print line


def main():
    data = load_data()
    options = args()

    if options.discipline:
        levels = discipline_levels(options.discipline, data)
        show_levels(levels)
    elif options.list:
        known = load_known(options.list)
        find_unused(known, data)

if __name__ == '__main__':
    main()
